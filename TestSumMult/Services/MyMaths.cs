﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestSumMult.Services
{
    public class MyMaths : ITestSumMult
    {
        public double MyMathMult(double a, double b)
        {
            return a * b;
        }

        public double MyMathSub(double a, double b)
        {
            return a - b;
        }

        public double MyMathSum(double a, double b)
        {
            return a + b;
        }
    }
}
