﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestSumMult.Services
{
    public interface ITestSumMult
    {
        double MyMathSum(double a, double b);
        double MyMathSub(double a, double b);

        double MyMathMult(double a, double b);
    }
}
