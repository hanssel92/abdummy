﻿using System;
using TestSumMult.Services;

namespace TestSumMult
{
    class Program
    {
        static void Main(string[] args)
        {

            double a = 10;
            double b = 0.5;
            double sum = 0;
            double sub = 0;
            double mult = 0;

            MyMaths myMaths = new MyMaths();
            mult = myMaths.MyMathMult(a, b);
            sum = myMaths.MyMathSum(a, b);
            sub = myMaths.MyMathSub(a, b);

            Console.WriteLine("The sum is: " + sum.ToString());
            Console.WriteLine("The subtract is: " + sub.ToString());
            Console.WriteLine("The multi is: " + mult.ToString());

            //ITestSumMult
        }
    }
}
